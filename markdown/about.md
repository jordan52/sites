---
{
    "title": "About",
    "shortTitle": "about",
    "summary": "",
    "created": "2014-12-31",
    "modified": "2014-12-31",
    "type":"general",
    "categories": [
        "general"
    ],
    "tags": [
    ]
}
---
I thought it might be fun to build something a little more dynamic than a pandoc markdown site. My company has gotten a lot of mileage out of javascript so I figured why not.
 
The first round is a simple express.js site that renders markdown content. The content contains metadata that lets the site know if it is a blog post, story, etc. The code is on [bitbucket](https://bitbucket.org/jordan52/sites). I want some dynamic stuff. I might start with the collaborative sketch thingy I wrote for my band's site. It'll be a port from javascript/postgres and it will be rad.

What about me? I like to write code, stories, and general nonsense. I'm outlining a YA novel (HA!) I snowboard, play guitar, sing (sort of), fix cars, cut grass, rehab houses, and run. I co-founded a startup. I like whisky. I am a father. I am studying statistics via finance because from what I can tell they're kind of pushing the art. I helped manage data for clinical trials, cardiovascular research studies, and did some work in neuroinformatics. So, yeah, I do a lot.
 
My current plan is to grow this site through the years. I hope you'll come back. If you want to contact me, go for it. I'm easy to find.
